/*
USE EvenKeel;
Author: Drew Kroft
Date: 04/19/2018
YouTrack Ticket #: EK-44
Notes: Update database table names, column names and foreign keys to match verbiage used for client facing vocabulary
*/
USE EvenKeel;
RENAME TABLE `Workout` TO `WorkoutActivities`;
RENAME TABLE `WorkoutSession` TO `Workouts`;
RENAME TABLE `LKWorkoutSessionType` TO `LKWorkoutType`;
ALTER TABLE `WorkoutActivities` DROP FOREIGN KEY `workoutactivities_ibfk_5`;
ALTER TABLE `Workouts` DROP FOREIGN KEY `workouts_ibfk_3`;
ALTER TABLE `WorkoutSets` DROP FOREIGN KEY `workoutsets_ibfk_3`;
ALTER TABLE `WorkoutActivities` CHANGE WorkoutID WorkoutActivityID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `WorkoutActivities` CHANGE WorkoutSessionID WorkoutID INT(11) UNSIGNED NOT NULL;
ALTER TABLE `WorkoutActivities` CHANGE WorkoutDescription WorkoutActivityDescription TEXT;
ALTER TABLE `Workouts` CHANGE WorkoutSessionID WorkoutID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `Workouts` CHANGE WorkoutSessionTypeID WorkoutTypeID INT(11) UNSIGNED NOT NULL;
ALTER TABLE `Workouts` CHANGE WorkoutSessionDescription WorkoutDescription TEXT;
ALTER TABLE `WorkoutSets` CHANGE WorkoutID WorkoutActivityID INT(11) UNSIGNED NOT NULL;
ALTER TABLE `LKWorkoutType` CHANGE WorkoutSessionTypeID WorkoutTypeID INT(11) UNSIGNED NOT NULL;
ALTER TABLE `LKWorkoutType` CHANGE WorkoutSessionTypeDescription WorkoutTypeDescription VARCHAR(60);
UPDATE `SystemAreaType` SET SystemAreaTypeDescription = 'Workouts' WHERE SystemAreaTypeID = 1;
UPDATE `SystemAreaType` SET SystemAreaTypeDescription = 'WorkoutActivities' WHERE SystemAreaTypeID = 2;
ALTER TABLE `WorkoutActivities` ADD CONSTRAINT `workoutactivities_ibfk_5` FOREIGN KEY (`WorkoutID`) REFERENCES `Workouts` (`WorkoutID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `Workouts` ADD CONSTRAINT `workouts_ibfk_3` FOREIGN KEY (`WorkoutTypeID`) REFERENCES `LKWorkoutType` (`WorkoutTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `WorkoutSets` ADD CONSTRAINT `workoutsets_ibfk_3` FOREIGN KEY (`WorkoutActivityID`) REFERENCES `WorkoutActivities` (`WorkoutActivityID`) ON DELETE NO ACTION ON UPDATE NO ACTION;